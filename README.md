This is to create a website used to track the changes of an analysis software using the capabilities of [JavaScript ROOT] and [GitLab CI].

As an example it is used to monitor [ttResFullHad], which is analysis framework used in 0-lep and 1-lep $`t\bar{t}`$ searches:  
<div align="center">
http://ttreshad.web.cern.ch/ttreshad/RTT

![ScreenShot]
</div>

[JavaScript ROOT]: https://root.cern/js/
[GitLab CI]: https://about.gitlab.com/product/continuous-integration/
[ttResFullHad]: https://gitlab.cern.ch/atlas-phys/exot/hqt/tt_tb_Resonances_Run2/ttResFullHad
[ScreenShot]: img/Screenshot.png